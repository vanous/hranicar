CREATE TABLE hosts (
    host_id INTEGER PRIMARY KEY,
    mac TEXT,
    vendor TEXT,
        name TEXT,
        ip TEXT,
        ap TEXT,
        rx TEXT,
        tx TEXT,
        snr TEXT,
    last_seen timestamp
, first_seen timestamp, blocked boolean, image TEXT);

CREATE TABLE records (
    record_id INTEGER PRIMARY KEY,
    mac TEXT,
        ip TEXT,
        ap TEXT,
    date timestamp
, blocked boolean);

CREATE TABLE accounting_history
                    (
                        ha text,
                        bi real,
                        bo real,
                        snap_date timestamp
                    );
CREATE TABLE accounting_totals (ha text, bi real,bo real);
