#!/usr/bin/env python
import urllib2 as ul
import sqlite3 as ps
from sqlite3 import OperationalError
import sys
import os.path
from datetime import date, datetime
os.chdir(os.path.dirname(os.path.abspath(__file__)))


PRINT_STATS_AT_END = False

LOCALNET = '192.168.20.'
MIKROTIK = LOCALNET+'1:88'

now=datetime.utcnow()

dl = None
dc = None
dr = None

def update_host(ha,bi,bo):
    hs = get_host(ha)
    tbi = int(bi)+hs['in']
    tbo = int(bo)+hs['out']
    dc.execute('UPDATE accounting_totals SET bi=?, bo=? WHERE ha=?',(tbi,tbo,ha,))
    dc.execute('INSERT INTO accounting_history VALUES(?,?,?,?)',(ha,str(bi),str(bo),now,)) 

def get_host(ha):
    fr = dc.execute("SELECT * FROM accounting_totals WHERE ha=?",(ha,)).fetchall()
    if len(fr) == 0:
        dc.execute("INSERT INTO accounting_totals VALUES (?,0,0)",(ha,))
        fr = dc.execute("SELECT * FROM accounting_totals WHERE ha=?",(ha,)).fetchall()
    return {'in': int(fr[0][1]),'out': int(fr[0][2])}

# Display help if user requests it
bInitArg = True
for arg in sys.argv:
    if bInitArg:
        bInitArg = False
    else:
        if arg=="-H" or arg=="--help":
            print "MikroTik RouterOS bandwidth accounting toolbox by Quinn Ebert"
            print ""
            print "USAGE:"
            print sys.argv[0]+" [--help|-H] [--readonly|-R] [--stats-lifetime|-A] [-D=<file>]"
            print ""
            print "            --help|-H   shows this help message and exits"
            print "        --readonly|-R   don't gather/reset latest accounting snapshot"
            print "  --stats-lifetime|-A   show A=>B lifetime byte/packet totals (forces -R)"
            print "            -D=<file>   use <file> for database, not the built-in default"
            print ""
            print "Without any arguments, the script talks to the MikroTik, gathers the latest"
            print "snapshot and updates the database as relevant."
            print ""
            print "If the -D=<file> argument isn't used, the default database file behaviour"
            print "is to create or open accounting.sqlite3 in the current working directory."
            sys.exit()
# Process any other command line args
READONLY_MODE = False
DATABASE_FILE = "accounting.sqlite3"
bInitArg = True
for arg in sys.argv:
    if bInitArg:
        bInitArg = False
    else:
        if arg=="-R" or arg=="--readonly":
            READONLY_MODE = True
        elif arg=="-A" or arg=="--stats-lifetime":
            READONLY_MODE = True
            PRINT_STATS_AT_END = True
        elif arg.startswith("-D="):
            DATABASE_FILE = arg.split("=")[1]

dl = ps.connect('../data/data.db',isolation_level=None)
dc = dl.cursor()
try:
    dr = dc.execute("SELECT * FROM accounting_totals")
except OperationalError:
    dc.execute("CREATE TABLE accounting_totals (ha text, bi real,bo real)")
try:
    dr = dc.execute("SELECT * FROM accounting_history")
except OperationalError:
    dc.execute('''CREATE TABLE accounting_history
                    (
                        ha text,
                        bi real,
                        bo real,
                        snap_date timestamp
                    )''')

if not READONLY_MODE:
    res = ul.urlopen(ul.Request('http://'+MIKROTIK+'/accounting/ip.cgi')).read().rstrip().split("\n")
    print len(res)
    pds = {}
    # Massage the data into a more useful format
    for rec in res:
        col = rec.split(" ")
        host_a = col[0]
        host_b = col[1]
        n_Byts = col[2]
        n_Pkts = col[3]
        if LOCALNET in host_a:
            if not host_a in pds:
                pds[host_a] = {}
                pds[host_a]['in']={}
                pds[host_a]['out']={}
                pds[host_a]['in']['bytes'] = 0
                pds[host_a]['in']['packets'] = 0
                pds[host_a]['out']['bytes'] = int(n_Byts)
                pds[host_a]['out']['packets'] = int(n_Pkts)
            else:
                pds[host_a]['out']['bytes'] += int(n_Byts)
                pds[host_a]['out']['packets'] += int(n_Pkts)
        elif LOCALNET in host_b:
            if not host_b in pds:
                pds[host_b] = {}
                pds[host_b]['in']={}
                pds[host_b]['out']={}
                pds[host_b]['out']['bytes'] = 0
                pds[host_b]['out']['packets'] = 0
                pds[host_b]['in']['bytes'] = int(n_Byts)
                pds[host_b]['in']['packets'] = int(n_Pkts)
            else:
                pds[host_b]['in']['bytes'] += int(n_Byts)
                pds[host_b]['in']['packets'] += int(n_Pkts)
    # Update DB
    #print pds
    for host in pds.iteritems():
            update_host(host[0],host[1]['in']['bytes'],host[1]['out']['bytes'])
else:
    print "[Read Only Mode Active]\n"

# Print statistics from DB
if PRINT_STATS_AT_END:
    fr = dc.execute("SELECT * FROM accounting_totals WHERE ha LIKE ? ORDER BY ha ASC",(LOCALNET + '%',)).fetchall()
    for fr_r in fr:
        (a,b,c) = fr_r
        e = round(float( float(b) / int(1024) / int(1024) ))
        f = round(float( float(c) / int(1024) / int(1024) ))
        print "%s\t%sMB\t%sMB" % (str(a), str(round(e,3)), str(round(f,3)),)

