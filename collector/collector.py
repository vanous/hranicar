import subprocess
import sqlite3
import xml.etree.ElementTree as ET
from lxml import etree
from datetime import date, datetime
import urllib2
import os.path
import smtplib
import credentials
import socket
import fcntl
import struct

#list of openwrt access point IPs + names

os.chdir(os.path.dirname(os.path.abspath(__file__)))

#define email settings in the credentials.py file
EMAIL=credentials.EMAIL
IFNAME="eth0"

aps=[
["192.168.20.238","loznice"],
["192.168.20.239","puda"]
]
bashNmapCommand = "sudo nmap -sP -n -oX - 192.168.20.0/24"

bashGetFirewallCommand="ssh -i /home/pi/.ssh/id_rsa2 vanous@192.168.20.1 ip firewall filter print"


now=datetime.utcnow()
new=[]

def get_assoc(ap,ap_name):
    output=None
    bashIwinfoCommand = "ssh -C root@%s iwinfo wlan0 assoclist" % ap
    process = subprocess.Popen(bashIwinfoCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    conn = sqlite3.connect('../data/data.db')

    if output is not None:
	if not (output in "No such wireless device"):
            if len(output.split("\n\n"))>1:
                for host in output.split("\n\n"):
                    if host is not "":
                        vendor=False
                        #print ap_name, host.split()
                        lines=host.split("\n")
                        #print lines
                        mac=lines[0].split()[0]
                        #ap=ap_name
                        snr=lines[0].split()[7].replace(")","")
                        rx=lines[1].split()[1]
                        tx=lines[2].split()[1]
                        ago=lines[0].split()[8]
                        if not len(conn.execute('select mac from hosts where mac=?;',(mac,)).fetchall()):
                            if mac not in new:
                                new.append(mac)
                        conn.execute('INSERT INTO records (mac,ap,date) values(?,?,?);',(mac,ap_name,now))
                        conn.execute('INSERT INTO hosts (mac,first_seen) select ?,? where not exists (select mac,first_seen from hosts where mac=?);',(mac,now,mac,))
                        res=conn.execute('SELECT mac, vendor from hosts where mac=?;',(mac,))
                        data=res.fetchall()[0]
                        res.close()
                        if data[1]:
                            vendor=data[1]
                        else:
                            print "remote call to macvendors"
                            vendor=urllib2.urlopen("https://api.macvendors.com/"+mac[:8]).read()
                        #vendor=urllib2.urlopen("https://api.macvendors.com/"+mac[:8]).read()
                        conn.execute('UPDATE hosts set rx=?,tx=?,snr=?,last_seen=?,ap=?,vendor=? where mac=?;',(rx,tx,snr,now,ap_name,vendor,mac))
                conn.execute('UPDATE hosts set ap=? where ip=?;',("{} clients".format(len(output.split("\n\n"))-1),ap,))
	else: #AP off
	    conn.execute('UPDATE hosts set ap="OFF" where ip=?;',(ap,))
    
    conn.commit()
    conn.close()


def get_aps():
    for ap in aps:
        get_assoc(ap[0],ap[1])

def get_nmap():
    output=None
    process = subprocess.Popen(bashNmapCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    #print output


    #tree = ET.parse(output)
    tree = ET.ElementTree(ET.fromstring(output))
    root = tree.getroot()
    conn = sqlite3.connect('../data/data.db')
    for i in root.iter('host'):
	if i.find('address/[@addrtype="mac"]') is not None:
	    ip = i.find('address/[@addrtype="ipv4"]').get('addr')
	    mac=i.find('address/[@addrtype="mac"]').get('addr')
            if not len(conn.execute('select mac from hosts where mac=?;',(mac,)).fetchall()):
                if mac not in new:
                    new.append(mac)
	    conn.execute('INSERT INTO records (mac,ip,ap,date) values(?,?,?,?);',(mac,ip,"nmap",now))
            conn.execute('INSERT INTO hosts (mac,first_seen) select ?,? where not exists (select mac,first_seen from hosts where mac=?);',(mac,now,mac,))
	    vendor=i.find('address/[@addrtype="mac"]').get('vendor')
            #print "vendor from nmap:", vendor
            if vendor is None:
                #print "vendor none"
                res=conn.execute('SELECT mac, vendor from hosts where mac=?;',(mac,))
                data=res.fetchall()[0]
                res.close()
                if data[1]:
                    vendor=data[1]
                else:
                    print "remote call to macvendors"
                    vendor=urllib2.urlopen("https://api.macvendors.com/"+mac[:8]).read()
                #print vendor[0]
                #vendor=vendor[0]
	    #print ip, mac, vendor
            #vendor=urllib2.urlopen("https://api.macvendors.com/"+mac[:8]).read()
	    conn.execute('UPDATE hosts set ip=?,vendor=?,last_seen=?,ap=? where mac=?;',(ip,vendor,now, "nmap",mac))

    conn.commit()
    conn.close()

def get_local():
    ip=([l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0])
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', IFNAME[:15]))
    mac=''.join(['%02x:' % ord(char) for char in info[18:24]])[:-1]
    mac=mac.upper()
    #print "local: ",mac,ip
    conn = sqlite3.connect('../data/data.db')
    conn.execute('INSERT INTO records (mac,ip,ap,date) values(?,?,?,?);',(mac,ip,"localhost",now))
    conn.execute('INSERT INTO hosts (mac,first_seen) select ?,? where not exists (select mac,first_seen from hosts where mac=?);',(mac,now,mac,))
    res=conn.execute('SELECT mac, vendor from hosts where mac=?;',(mac,))
    data=res.fetchall()[0]
    res.close()
    if data[1]:
        vendor=data[1]
    else:
        print "remote call to macvendors"
        vendor=urllib2.urlopen("https://api.macvendors.com/"+mac[:8]).read()
    conn.execute('UPDATE hosts set ip=?,vendor=?,last_seen=?,ap=? where mac=?;',(ip,vendor,now,"localhost", mac))

    conn.commit()
    conn.close()

def get_firewall():
    output=None
    process = subprocess.Popen(bashGetFirewallCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    conn = sqlite3.connect('/home/pi/bin/hranicar/data/data.db')
    conn.execute('UPDATE hosts set blocked=0;')
    for rule in output.split("\n\n"):
        for line in rule.split("\n"):
            if "action=drop" in line:
                mac=line.split()[2].split("=")[1]
                conn.execute('INSERT INTO records (mac,blocked,ap,date) values(?,?,?,?);',(mac,True,"firewall",now))
                conn.execute('INSERT INTO hosts (mac,first_seen) select ?,? where not exists (select mac,first_seen from hosts where mac=?);',(mac,now,mac,))
                conn.execute('UPDATE hosts set blocked=? where mac=?;',(True,mac))

    conn.commit()
    conn.close()

def submit_new():
    #print "new: ", new
    TEXT=""
    if new:
        if EMAIL:
            conn = sqlite3.connect('../data/data.db')
            FROM = EMAIL[0]
            TO = [EMAIL[1]] #must be a list
            SUBJECT = "Hranicar: %s new host(s) detected!" % len(new)
            for noob in new:
                mac, vendor=conn.execute('select mac,vendor from hosts where mac=?;',(noob,)).fetchall()[0]
                TEXT += "\n%s %s" % (mac,vendor)

            # Prepare actual message
            message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
            """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
            try:
                server = smtplib.SMTP(EMAIL[2])
                server.sendmail(FROM, TO, message)
                server.close()
            except:
                pass





get_nmap()
get_aps()
get_firewall()
get_local()
submit_new()
