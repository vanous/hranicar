# -*- coding: utf-8 -*-
import web
import datetime

db = web.database(dbn='sqlite', db='../data/data.db')



def get_posts():
    w=dict(rang=datetime.datetime.now() - datetime.timedelta(days=1))
    return db.select('records', w,order='record_id DESC',where="date>$rang")
    #return db.select('records')

def get_post(mac):
    return db.select('hosts',where="mac=$mac",vars=locals())[0]

def get_records(_mac,_days=1):
    print "mac: ", _mac
    w=dict(rang=datetime.datetime.now() - datetime.timedelta(days=_days))
    w['_mac']=_mac
    try:
        if _mac=="all":
            #return db.select('records', w,order='record_id DESC',where="date>$rang").list()
            #return db.query('SELECT records.*,hosts.name  FROM records left join hosts on records.mac = hosts.mac where date>$rang;',w).list()
            r1=db.query('SELECT records.*,datetime(records.date,"+1 hour") as dato,hosts.name FROM records left join hosts on records.mac = hosts.mac where date>$rang;',w).list()
            return r1
        else:
            #return db.select('records', w,order='record_id DESC',where="date>$rang and mac=$_mac").list()
            r1=db.query('SELECT records.*,hosts.name, datetime(records.date,"+1 hour") as dato FROM records left join hosts on records.mac = hosts.mac where date>$rang and records.mac=$_mac;',w).list()
            return r1
    except IndexError:
	return None
    #return db.select('records')

def get_records2(_ip,_days=1):
    w=dict(rang=datetime.datetime.now() - datetime.timedelta(days=_days))
    w['_ip']=_ip
    try:
        if _ip=="all":
            r2=db.query('SELECT accounting_history.*,hosts.name, datetime(accounting_history.snap_date,"+1 hour") as dato from accounting_history left join hosts on accounting_history.ha=hosts.ip where snap_date > $rang',w).list()
            return r2
        else:
            r2=db.query('SELECT accounting_history.*,hosts.name, datetime(accounting_history.snap_date,"+1 hour") as dato from accounting_history left join hosts on accounting_history.ha=hosts.ip where snap_date > $rang  and accounting_history.ha=$_ip',w).list()
            return r2
    except IndexError:
	return None
    #return db.select('records')


def get_hosts():
    hosts_ap=db.select('hosts', order='last_seen DESC',where="name like '%%%AccessPoint%%%'").list()
    hosts_blocked=db.select('hosts', order='last_seen DESC',where="(name is null or name not like '%%%AccessPoint%%%') and blocked = 1").list()
    hosts_clients=db.select('hosts', order='last_seen DESC',where="(name is null or name not like '%%%AccessPoint%%%' )and blocked = 0").list()
    hosts_all=hosts_ap+ hosts_blocked+ hosts_clients
    return hosts_all


def new_post(title, text):
    db.insert('entries', title=title, content=text, posted_on=datetime.datetime.utcnow())


def del_post(_id):
    db.delete('entries', where="id=$_id", vars=locals())


def update_post(mac,name,image):
    db.update('hosts', where="mac=$mac", vars=locals(), name=name,image=image)
