# -*- coding: utf-8 -*-
""" 使用 web.py 0.3
"""
import web
import model
import os,random
import subprocess
from datetime import date, datetime
import sqlite3

urls = (
    '/', 'Index',
    '/favicon.ico', 'Favicon',
    '/view', 'View',
    '/detail/(.*)', 'Detail',
    '/detail2/(.*)', 'Detail2',
    '/block','Block',
    '/edit/(.*)','Edit'
)


t_global = {
    'dateStr': web.datestr
}
render = web.template.render('templates', base='base', globals=t_global)
#render = web.template.render('templates', globals=t_global)

class Favicon(object):
        def GET(self):
            raise web.seeother("/static/images/ranger.ico")

class Index(object):
    def GET(self):
        hosts = model.get_hosts()
        return render.index(hosts)


class View(object):
    def GET(self):
        hosts = model.get_hosts()
        return render.view(hosts)


class Edit(object):
    form = web.form.Form(
        web.form.Textbox('name', web.form.notnull, size=30, description="Name:"),
        web.form.Dropdown('image',args=[]),
        web.form.Button('Post entry'),
    )

    def GET(self,mac):
        c=[]
        a=os.listdir("./static/images")
        for b in a:
            c.append((b,b))
        post = model.get_post(mac)
        form = self.form()
        form.image.args=c
        form.fill(post) 
        return render.edit(post, form)

    def POST(self,mac):
        c=[]
        a=os.listdir("./static/images")
        for b in a:
            c.append((b,b))
        post = model.get_post(mac)
        form = self.form()
        form.image.args=c
        if not form.validates():
            return render.edit(post, form)
        model.update_post(mac,form.d.name,form.d.image)
        #raise web.seeother('/detail/'+mac)
        raise web.seeother('/')


class Block(object):
    def POST(self):
        user_data = web.input()
        final="ssh -i /home/pi/.ssh/id_rsa2 vanous@192.168.20.1".split()
        nl="\n".join('  ').replace(" ","")
        poslat=False
        if len(user_data.allow) > 0:
            allow=user_data.allow.split(",")
            if len(allow)>0:
                poslat=True
                for item in allow:
                    a="ip firewall filter remove [find comment=%s]" % item
                    for v in a.split():
                        final.append(v)
                    final.append(nl)


        if len(user_data.deny) > 0:
            deny=user_data.deny.split(",")
            if len(deny) > 0:
                poslat=True
                for item in deny:
                    b="ip firewall filter add chain=forward src-mac-address=%s action=drop comment=%s" % (item,item)
                    for v in b.split():
                        final.append(v)
                    final.append(nl)
        
            
        
        print final
        if poslat:
            process = subprocess.Popen(final, stdout=subprocess.PIPE)
            output, error = process.communicate()
            print "output of poslat: ", output
            bashGetFirewallCommand="ssh -i /home/pi/.ssh/id_rsa2 vanous@192.168.20.1 ip firewall filter print"
            process = subprocess.Popen(bashGetFirewallCommand.split(), stdout=subprocess.PIPE)
            output, error = process.communicate()
            #print "cti stav: ", output
            conn = sqlite3.connect('/home/pi/bin/hranicar/data/data.db')
            conn.execute('UPDATE hosts set blocked=0;')
            now=datetime.utcnow()
            for rule in output.split("\n\n"):
                for line in rule.split("\n"):
                    if "action=drop" in line:
                        mac=line.split()[2].split("=")[1]
                        conn.execute('INSERT INTO records (mac,blocked,ap,date) values(?,?,?,?);',(mac,True,"firewall",now))
                        conn.execute('INSERT INTO hosts (mac,first_seen) select ?,? where not exists (select mac,first_seen from hosts where mac=?);',(mac,now,mac,))
                        conn.execute('UPDATE hosts set blocked=? where mac=?;',(True,mac))
            
            conn.commit()
            conn.close()


class Detail2(object):

    def GET(self, _ip,_days=1):
        account = model.get_records2(_ip,_days)
        #print "post: ", len(post), post,_mac,_days
        if account is not None and (len(account)>0):
            return render.detail2(account)
        else:
            if _days==1:
                #print "opakuj", _mac, _days
                return self.GET(_ip,7)
            else:
                #print "konec"
                return render.empty(_ip)

class Detail(object):

    def GET(self, _mac,_days=1):
        post = model.get_records(_mac,_days)
        #print "post: ", len(post), post,_mac,_days
        if post is not None and (len(post)>0):
            return render.detail(post)
        else:
            if _days==1:
                #print "opakuj", _mac, _days
                return self.GET(_mac,7)
            else:
                #print "konec"
                return render.empty(_mac)

app = web.application(urls, globals())


if __name__ == '__main__':
    app.run()
