# Local network host discoverer, collector and blocker

* Based on web.py
* uses jQuery Mobile
* usage charts by Chart.js

List devices (more screenshots below):

![List devices](list_devices.png)

## Provides
* continuous network discovery (via cron based collector)
* fetching traffic accounting data from RouterOS
* remote firewall rules settings on RouterOS
* links devices to Access Points
* shows active devices, history, signal strength
* shows charts of when devices were online
* shows charts of devices' data usage
* provides means to allow/deny devices via adding/removing router's firewall rules
* show and send email alert when new devices appear - discover if someone is using your wifi/network without you knowing
* edit names and icons of your devices

## Hosts blocking/unblocking:
* swipe left/right (on multiple hosts) and then hit the top-right button. Little obfuscation of this process is for the teenagers in the house

## Usage:
* edit collector.py - if you have openWrt based AP, add them to the list, add RouterOS IP
* edit credentials.py to add email address of a sender, receiver and smtp server
* edit accounting.py to set RouterOS IP and port
* in the data directory, create empty database: sqlite3 data.db < schema.sql
* run collector.py from cron every xx (10) minutes
* run accounting.py from cron. I do it now every minute, but more is better
* add icon images to web/static/images , mainly true.png, false.png, new.png and more
* start python blog.py
* go to host IP:8080 via browser (mobile, for best experience)
* if you like to eliminate address with port 8080 and don't lite to run this as root, do port redirect:

`iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080`

## Requires:
* openWRT based access points for wifi associated lists data
* RouterOS mikrotik router for firewalling and traffic data accounting
* ssh keys for passwordless login to the above
* nmap for network discovery
* some python dependencies: webpy, subprocess, datetime, sqlite3

## Notes:
* the base.html contains js based console for development on mobile, commented out by default

## Screenshots

Block/Unblock devices:

![Block/Unblock devices](manage_devices.png)

Edit device:

![Edit device](edit_device.png)

View usage hours of all devices:

![Plot usage](chart_big.png)

View single device usage hours:

![View single device](chart_single.png)

View single device traffic:

![View single device traffic](chart_traffic.png)
